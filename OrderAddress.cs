﻿namespace NoniB.Ecom.Model
{
    public class OrderAddress : IOrderAddress
    {
        public AddressType AddressType { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string AttentionOf { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

       

    }
}
