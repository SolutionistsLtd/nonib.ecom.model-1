﻿using System;

namespace NoniB.Ecom.Model
{
    public interface IOrderVoucher
    {
        DateTime? ExpiryDate { get; set; }
        string GiftCardNo { get; set; }
        string GiftCardPin { get; set; }
        string GiftCardType { get; set; }
        decimal GiftCardValue { get; set; }
        string GiftEmail { get; set; }
        string GiftMessage { get; set; }
        string Name { get; set; }
        string OrderLineId { get; set; }
    }
}