﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class PricingSku
    {
        /// <summary>
        /// List of skus within the price change document. See <see cref="Ecom.API.Data.Entity.PricingSku"/> for object schema
        /// </summary>
        public string SkuId { get; set; }
        public decimal NewPriceInc { get; set; }
        public decimal NewPriceExc { get; set; }
        /// <summary>
        /// price_status_code	price_status_desc
        /// CLR                 Clearance
        /// CON                 Consignment
        /// FBR                 Fabric
        /// FIN                 Final Clearance
        /// MD1                 Markdown 1
        /// MD2                 Markdown 2
        /// MDC                 MD Cancellation
        /// MU                  Mark Up
        /// MUC                 MU Cancellation
        /// PRM                 Promotional
        /// REG                 Regular
        /// </summary>
        public string NewPriceStatus { get; set; }
        public string CurrencyCode { get; set; }

    }
}
