﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class CustomerAddress
    {
        /// <summary>
        /// Address Type should be Billing or Delivery
        /// </summary>
        public AddressType AddressType { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string CountryCode { get; set; }  //This is the ISO code

        public InstanceStatus DataStatus { get; set; }
    }

  
}
