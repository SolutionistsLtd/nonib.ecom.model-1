﻿using System;
using System.Collections.Generic;
using System.Text;
namespace NoniB.Ecom.Model
{
    public class IntegrationMessageUpdate
    {
        public string StyleCode { get; set; }
        public MessageUpdateStatus Status { get; set; }
        public List<string> IntegrationMessages { get; set; }
    }
    public enum MessageUpdateStatus
    {
        Received = 1,
        Processed = 2,
        Failed = 3
    }
}