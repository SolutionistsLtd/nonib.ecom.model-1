﻿namespace NoniB.Ecom.Model
{
    public class OrderCheck
    {
        public string OrderNumber { get; set; }
        public string OrderReference { get; set; }
        public bool CanCancel { get; set; }
        public string Status { get; set; }
    }
}