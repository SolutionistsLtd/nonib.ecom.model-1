﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class Customer : ICustomer
    {
        /// <summary>
        /// Customer Number (code)  –  plastic card identification number – this identifies Sam Smith’s Rockmans card R12345678
        /// </summary>
        public string CustomerNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Source { get; set; }  //Online, MemberRegistration
        public string SourceSite { get; set; }
        public string RewardProgram { get; set; }
        public decimal RewardBalance { get; set; }
        public string TierLevel { get; set; }
        public List<CustomerAddress> Addresses { get; set; }
        //public List<CustomerAttribute> Attributes { get; set; }
        public List<CustomerBrandFlag> BrandFlags { get; set; }
        //public List<CustomerGeneralFlag> GeneralFlags { get; set; }
        public List<PromotionalVoucher> PromotionalVouchers { get; set; }

        public InstanceStatus DataStatus { get; set; }

    }
}
