﻿using System.Collections.Generic;

namespace NoniB.Ecom.Model
{
    public class OrderItem : IOrderItem
    {
        public string SkuId { get; set; }

        /// <summary>
        /// Should be a 1. Multiple qtys of same product should not be grouped. Reason - Split Shipping
        /// </summary>
        
        public int QuantityOrdered { get; set; }

        /// <summary>
        /// Ecommerce Order Line ID
        /// </summary>
        
        public string OrderLineId { get; set; }

        /// <summary>
        /// inc GST
        /// </summary>
        
        public decimal UnitRetail { get; set; }

        /// <summary>
        /// GST Amount
        /// </summary>
        
        public decimal UnitTax { get; set; }

        /// <summary>
        /// Discount Amount
        /// </summary>
        
        public decimal LineDiscount { get; set; }

        /// <summary>
        /// Discount Reasons
        /// </summary>
        
        public List<string> DiscountReason { get; set; }

        /// <summary>
        /// Rebates, Promo Codes
        /// DB: OrderReduction
        /// </summary>
        public List<string> CouponCode { get; set; }
        public string ReturnReason { get; set; }

        public string ProductCode { get; set; }

        public bool IsDropShip { get; set; }

        public string SupplierName { get; set; }
        public string FulfillmentStore { get; set; } // This will be the store number that order will be collected from
        public string OrderItemStatus { get; set; } // This will click and collect line status

    }
}
