﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class LocationAttribute
    {
         public string AttributeCode { get; set; }
         public string AttributeLabel { get; set; }
         public string AttributeSetCode { get; set; }
         public string AttributeSetLabel { get; set; }
    }
}
