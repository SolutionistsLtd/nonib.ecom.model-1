﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class Location
    {
        /// <summary>
        /// Key value of the location. Used in most calls and other objects.
        /// </summary>

        public string StoreNumber { get; set; }

        public string StoreName { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }

        /// <summary>
        /// City/Suburb
        /// </summary>

        public string City { get; set; }

        public string State { get; set; }
        public string Country { get; set; }
        public string Postcode { get; set; }
        public string Phone { get; set; }

        /// <summary>
        /// A true value would signify the location is open for trade.
        /// </summary>

        public bool Open { get; set; }

        /// <summary>
        /// True means it could SOH or Allocations against the location
        /// </summary>

        public bool Active { get; set; }

        public string HierarchyCode { get; set; }
        public string LatLong { get; set; }
        public List<LocationAttribute> LocationAttributes { get; set; }

        public List<OpeningHour> OpeningHours { get; set; }
    }


    public class OpeningHour
    {
        public DayOfWeek DayOfWeek { get; set; }
        public DateTime OpeningTime { get; set; } //Only use the time part of this.  Date will be 1900-01-01
        public DateTime ClosingTime { get; set; }  //Only use the time part of this.  Date will be 1900-01-01
        public bool IsPublicHoliday { get; set; }
        public string HolidayDescription { get; set; }
    }

}
